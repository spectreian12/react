import axios from "axios";

const instance = axios.create({
  baseURL: "https://backendsoren.onrender.com",
});

export default instance;
