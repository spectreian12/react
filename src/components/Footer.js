import React from "react";
import { Navbar } from "react-bootstrap";
import "./Footer.css";

function Footer() {
  return (
    <div className="main-footer">
      <div className="container">
        <div className="row">
          {/* column1(CUSTOMER SERVICE) */}
          <div className="col">
            <h4>Customer Service</h4>
            <ul className="footerDetails">
              <li>
                <Navbar.Text>
                  <a href="tel:09533175402">Call Support</a>
                </Navbar.Text>
              </li>
              <li>
                <Navbar.Text>
                  <a href="#link">Chat Support</a>
                </Navbar.Text>
              </li>
              <li>
                <Navbar.Text>
                  <a href="#link">Warranty Information</a>
                </Navbar.Text>
              </li>
            </ul>
          </div>

          {/* column4(LOCATION) */}
          <div className="col">
            <h4>Location</h4>
            <ul className="footerDetails">
              <li>
                <Navbar.Text>
                  <a
                    href="https://goo.gl/maps/1tG45GXuyxcjxXeM7"
                    target="_blank"
                  >
                    Downtown Dumaguete
                  </a>
                </Navbar.Text>
              </li>
              <li>
                <Navbar.Text>
                  <a
                    href="https://goo.gl/maps/L6Uxxvn3MY4Hu6zX9"
                    target="_blank"
                  >
                    Ground Floor, Cang's Inc
                  </a>
                </Navbar.Text>
              </li>
            </ul>
          </div>

          {/* column3(INFO AND NEWS) */}
          <div className="col">
            <h4>News and Info</h4>
            <ul className="footerDetails">
              <li>Events</li>
            </ul>
          </div>
        </div>
        <hr />
        <div className="row">
          <p className="col-sm">
            &copy;{new Date().getFullYear} Lazahoppee | All right reserved |
            Term of Service | Privacy
          </p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
