const categories = [
  {
    name: "Phones",
    img: "https://images.unsplash.com/photo-1666238854502-b2eb911af153?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGdvb2dsZSUyMHBpeGVsJTIwcGhvbmV8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60",
  },
  {
    name: "Laptops",
    img: "https://images.unsplash.com/photo-1587613842560-0816bd27a096?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fHN1cmZhY2UlMjBsYXB0b3B8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60",
  },

  {
    name: "Audio",
    img: "https://images.unsplash.com/photo-1617766376513-148515e5d3b8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8c29ueSUyMGF1ZGlvfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600&q=60",
  },

  {
    name: "Camera",
    img: "https://images.unsplash.com/photo-1606986628470-26a67fa4730c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
  },
];

export default categories;
